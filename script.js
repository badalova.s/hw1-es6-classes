// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Можливість обьектів настлідувати властивості один одного

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// super()  викликається для того щоб не повторювати частину конструктора класу батька


class Employee {
    constructor(name,age,salary) {
        this.name=name
        this.age=age
        this.salary=salary
    }

    
    set name(value) {
        if (value.length <= 2) {
          console.log("Введіть корректне ім'я")
          return;
        }
        this._name = value;
            
      }
      get name() {
        return this._name
      }
   
    
    set age(value) {
        if (value<=18) {
          console.log("Введіть корректно Ваш вік")
          return;
        }
        this._age = value;
      } 
      get age() {
        return this._age
      }
      
    get salary() {
        return `${this._salary} $`
      }

}

class Programmer extends Employee {
    constructor(name,age,salary,lang){
    super(name,age,salary)
    this.lang=lang
    } 
    set salary (value){
        this._salary=value
    }
    get salary() {
    
        return `${this._salary*3} $`
    }

}
let language=["eng","ua"]
let user =new Programmer("sab",0,355,language)
console.log(user)
console.log(user.age)
console.log(user.name)
console.log(user.salary)
console.log(user.lang)
let user1 =new Programmer("s",20,355,language)
console.log(user1)
console.log(user1.age)
console.log(user1.name)
console.log(user1.salary)
let user2 =new Programmer("sabina",20,1000,language)
console.log(user2)
console.log(user2.age)
console.log(user2.name)
console.log(user2.salary)